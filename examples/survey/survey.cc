#include <nanomsg/nn.h>
#include <nanomsg/survey.h>

#include <sak/proto.hh>

#include <unistd.h>

#include <string>
#include <cassert>
#include <cstdio>

static const std::string SERVER = "server";
static const std::string CLIENT = "client";

int server(const std::string &url)
{
    int sock = nn_socket(AF_SP, NN_SURVEYOR);
    assert(sock >= 0);
    assert(nn_bind(sock, url.c_str()) >= 0);
    sleep(1); // wait for connections

    printf("[s] >> sending survey request\n");
    std::string data = "name";
    int bytes = nn_send(sock, data.c_str(), data.size() + 1, 0);
    assert (bytes == data.size() + 1);

    for (;;) {
        char *buf = NULL;
        int bytes = nn_recv(sock, &buf, NN_MSG, 0);
        if (bytes == ETIMEDOUT) {
            break;
        }

        if (bytes >= 0) {
            printf("[s] << \"%s\" survey response\n", buf);
            nn_freemsg(buf);
        }
    }

    return nn_shutdown (sock, 0);
}

int client (const std::string &url, const std::string &name)
{
    int sock = nn_socket(AF_SP, NN_RESPONDENT);
    assert(sock >= 0);
    assert(nn_connect(sock, url.c_str()) >= 0);

    for (;;) {
        char *buf = NULL;
        int bytes = nn_recv(sock, &buf, NN_MSG, 0);

        if (bytes >= 0) {
            printf("[%s] << \"%s\" survey request\n", name.c_str(), buf);
            nn_freemsg(buf);
            printf("[%s] >> survey response\n", name.c_str());
            int bytes = nn_send(sock, name.c_str(), name.size() + 1, 0);
            assert(bytes == name.size() + 1);
        }
    }

    return nn_shutdown(sock, 0);
}

int main (const int argc, const char **argv)
{
    if (argc >= 2 && SERVER == argv[1]) {
        return server(argv[2]);
    } else if (argc >= 3 && CLIENT == argv[1]) {
        return client(argv[2], argv[3]);
    } else {
        fprintf(stderr, "usage: survey %s|%s <url> <arg> ...\n", SERVER.c_str(), CLIENT.c_str());
        return -1;
    }
}

