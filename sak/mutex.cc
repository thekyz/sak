#include "log.hh"
#include "mutex.hh"

#include <pthread.h>

namespace sak { namespace mutex {

    scope_lock::scope_lock(pthread_mutex_t &mutex)
        : m_mutex(mutex), m_locked(true)
    {
        if (pthread_mutex_lock(&mutex) != 0) {
            sak_log_error("failed to lock scope mutex");
            m_locked = false;
        }
    }

    scope_lock::~scope_lock()
    {
        if (m_locked) {
            (void)pthread_mutex_unlock(&m_mutex);
        }
    }

} }

