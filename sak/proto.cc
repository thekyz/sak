#include <google/protobuf/util/json_util.h>

#include <nanomsg/nn.h>
#include <nanomsg/survey.h>

#include <pthread.h>

#include "proto.hh"
#include "mutex.hh"
#include "log.hh"

#include <sstream>
#include <string>
#include <map>
#include <vector>

namespace sak { namespace proto {

struct context
{
    context(connection *connection, void *user_ctx)
        : m_connection(connection), m_user_ctx(user_ctx) { }

    connection *m_connection;
    void *m_user_ctx;
};

pthread_mutex_t proxy::s_lock = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP;
proxy proxy::s_instance;

void proxy::register_connection(const google::protobuf::Message *msg, connection *conn)
{
    sak::mutex::scope_lock lock(s_lock);

    m_connections[conn] = msg->GetDescriptor()->full_name();
}

void proxy::unregister_connection(connection *conn)
{
    sak::mutex::scope_lock lock(s_lock);

    m_connections.erase(conn);
}

nt_listener *proxy::get_listener(const std::string &msg_type)
{
    sak::mutex::scope_lock lock(s_lock);

    for (std::map<connection *, std::string>::iterator it = m_connections.begin(); it != m_connections.end(); ++it) {
        if (it->second == msg_type) {
            return it->first->m_listener;
        }
    }

    return NULL;
}

std::vector<std::string> proxy::detailed_connections()
{
    sak::mutex::scope_lock lock(s_lock);

    unsigned offset = 0;
    for (std::map<connection *, std::string>::iterator it = m_connections.begin(); it != m_connections.end(); ++it) {
        offset = MAX(it->first->m_url.size(), offset);
    }

    std::vector<std::string> c;
    for (std::map<connection *, std::string>::iterator it = m_connections.begin(); it != m_connections.end(); ++it) {
        std::string d = "";
        d += std::string("[") + (it->first->m_connection_type == NN_RESPONDENT ? "c" : "s") + "]";
        d += " " + it->first->m_url;
        d.append(offset - it->first->m_url.size(), ' ');
        d += " << " + it->second;
        c.push_back(d);
    }

    return c;
}

connection::connection(int connection_type, std::string url, nt_listener *listener)
    : m_connection_type(connection_type), m_url(url), m_listener(listener), m_socket(0), m_state(UNINITIALIZED)
{
    pthread_mutexattr_t attr;

    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
    if (pthread_mutex_init(&m_listener_lock, &attr) != 0) {
        sak_log_error("sak::error: failed to initialize listener mutex");
        abort();
    }

    safe_open();

    proxy::instance()->register_connection(m_listener->msg(), this);
}

void connection::safe_open()
{
    if (m_state != UNINITIALIZED) {
        return;
    }

    m_socket = nn_socket(AF_SP, m_connection_type);
    if (m_socket < 0) {
        sak_log_error("sak::proto: failed to initialize socket - %s", strerror(errno));
        abort();
    }

    if (m_connection_type == NN_SURVEYOR) {
        m_endpoint_id = nn_bind(m_socket, m_url.c_str());
    } else {
        m_endpoint_id = nn_connect(m_socket, m_url.c_str());
    }

    if (m_endpoint_id < 0) {
        sak_log_error("sak::proto: failed to bind to socket - %s", strerror(errno));
        abort();
    }

    m_state = OPENED;
    return;
}

void connection::safe_close()
{
    if (m_state != OPENED) {
        return;
    }

    for (;;) {
        int err = nn_shutdown(m_socket, m_endpoint_id);
        if (err != EINTR) {
            break;
        }
    }

    m_state = UNINITIALIZED;
}

int connection::send(const google::protobuf::Message *msg, void *user_ctx)
{
    if (m_state != OPENED) {
        sak_log_error("sak::proto: cannot send a msg on a non opened channel");
        return -1;
    }

    std::string buffer = msg->SerializeAsString();
    if (buffer.empty()) {
        sak_log_error("sak::proto: failed to pack message");
        return -1;
    }

    return nn_send(m_socket, buffer.data(), buffer.size(), 0);
}

connection::~connection()
{
    proxy::instance()->unregister_connection(this);
    safe_close();
}

bool nt_server::is_connected()
{
    //TODO
    return true;
}

bool nt_client::is_connected()
{
    //TODO
    return true;
}

} }

