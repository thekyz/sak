#pragma once

#include <pthread.h>

namespace sak { namespace mutex {

/*!
 * \brief Scope mutex class.
 */
class scope_lock
{
    public:
        explicit scope_lock(pthread_mutex_t &mutex);
        virtual ~scope_lock();

    private:
        pthread_mutex_t &m_mutex;
        bool m_locked;
};

} }

