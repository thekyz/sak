#include <google/protobuf/util/json_util.h>

#include "proto.hh"
#include "proto_injector.hh"

#include <sstream>
#include <string>

namespace sak::yproto {

    static std::string s_service_name = "yproto";

    class dbg {
        public:
            int init();

            static void help(void *cookie);

            static void help_send(void *cookie);
            static int cmd_send(char *cmd, char *args, void *cookie);

            static void help_connections(void *cookie);
            static int cmd_connections(char *cmd, char *args, void *cookie);
    };

}

yproto::injector::injector(yproto::nt_listener *l)
    : peer("yproto::dbg::injector"), m_has_response(false), m_response_event(NULL), m_listener(l), m_user_ctx(NULL)
{
    l->on_connect(this);
}

yproto::injector::~injector()
{
    if (m_has_response) {
        m_listener->on_sent(YEVENT_OCCURED, m_user_ctx);
    }

    if (m_response_event) {
        yevent_delete(m_response_event);
    }

    m_listener->on_disconnect(this);
}

void yproto::injector::on_event(yevent_reason_t reason, void *cookie)
{
    yproto::injector *origin = static_cast<yproto::injector *>(cookie);

    if (origin->m_has_response) {
        origin->m_listener->on_sent(reason, origin->m_user_ctx);

        origin->m_has_response = false;
    }

    origin->m_response_event = NULL;
}

yevent_t *yproto::injector::send_response(const google::protobuf::Message *msg, yipc_message_handle_t yipc_msg_handle, void *user_ctx)
{
    if (m_response_event != NULL) {
        ylog_notag_printf(0, YLOG_WARNING, "yproto_dbg: second response on same request");
        return NULL;
    }

    m_response_event = yevent_new(on_event, this);
    if (m_response_event == NULL) {
        ylog_notag_printf(0, YLOG_ERROR, "yproto: failed to allocate memory for event");
        return NULL;
    }

    std::string buffer = msg->SerializeAsString();
    if (buffer.empty()) {
        ylog_notag_printf(0, YLOG_ERROR, "yproto: failed to pack message");
        return NULL;
    }

    google::protobuf::util::JsonPrintOptions options;
    options.add_whitespace = true;

    google::protobuf::util::MessageToJsonString(*msg, &m_response_json, options);
    m_has_response = true;

    return m_response_event;
}

yevent_t *yproto::injector::send_request(const google::protobuf::Message *msg, void *user_ctx)
{
    return NULL;
}

yevent_t *yproto::injector::send_oneway(const google::protobuf::Message *msg, void *user_ctx)
{
    return NULL;
}


void yproto::dbg::help(void *cookie)
{
    ydbg_printf("%s help:\n", s_service_name.c_str());
    ydbg_printf("---------------\n");
    ydbg_printf("* send         send a message to a channel\n");
    ydbg_printf("* connections  print the list of protobuf connections\n");
}

void yproto::dbg::help_send(void *cookie)
{
    ydbg_printf("%s help: send\n", s_service_name.c_str());
    ydbg_printf("---------------------\n");
    ydbg_printf("\n");
    ydbg_printf("Usage: %s send <msg> <json>\n", s_service_name.c_str());
    ydbg_printf("   <msg> should refer to the name of a registered protobuf message\n");
    ydbg_printf("   <json> should describe the content of a registered protobuf message\n");
    ydbg_printf("\n");
    ydbg_printf("Example:\n");
    ydbg_printf("   yproto send blueprint.Wrapper { \"get_line_config_request\":{ \"board\":0, \"config\":[{ \"id\":0 }, { \"id\":1 }] } }\n");
}

int yproto::dbg::cmd_send(char *cmd, char *args, void *cookie)
{
    std::vector<std::string> tokens = tokenize_input(const_cast<const char *>(args));

    if (tokens.size() < 2) {
        ydbg_printf("not enough arguments for yproto send\n");
        return -1;
    }

    std::string msg_type = tokens[0];
    std::string json_msg = concat_tokens(tokens, 1);

    yproto::nt_listener *l = yproto::proxy::instance()->get_listener(msg_type);
    if (l == NULL) {
        ydbg_printf("msg type '%s' is not available for current connections\n", msg_type.c_str());
        return -1;
    }

    ydbg_printf(" -->> %s %s\n", msg_type.c_str(), json_msg.c_str());
    yproto::injector fake_peer(l);
    std::string err = l->inject(json_msg, &fake_peer, YIPC_MSG_TYPE_REQUEST);
    if (!err.empty()) {
        ydbg_printf("%s\n", err.c_str());
        return -1;
    }

    if (fake_peer.has_response()) {
        std::istringstream iss(fake_peer.response_json());

        // iterate by line to circumvent buffer size limit in ydbg
        for (std::string line; std::getline(iss, line); ) {
            ydbg_printf("%s\n", line.c_str());
        }
    }

    return 0;
}

void yproto::dbg::help_connections(void *cookie)
{
    ydbg_printf("%s help: connections\n", s_service_name.c_str());
    ydbg_printf("---------------------\n");
    ydbg_printf("\n");
    ydbg_printf("Usage: %s connections\n", s_service_name.c_str());
}

int yproto::dbg::cmd_connections(char *cmd, char *args, void *cookie)
{
    std::vector<std::string> connections = yproto::proxy::instance()->detailed_connections();

    for (std::vector<std::string>::iterator it = connections.begin(); it != connections.end(); ++it) {
        ydbg_printf("   %s\n", (*it).c_str());
    }

    return 0;
}

int yproto::dbg::init()
{
    if (ydbg_link_module(s_service_name.c_str(), help, this) != 0) {
        return -1;
    }

  #define __ydbg_reg(__c)   if (ydbg_link_cmd(s_service_name.c_str(), # __c, help_ ## __c, cmd_ ## __c, this) != 0) { return -1; }

    __ydbg_reg(send);
    __ydbg_reg(connections);

    return 0;
}

int yproto_dbg_init()
{
    static yproto::dbg s_yproto_dbg;
    return s_yproto_dbg.init();
}

