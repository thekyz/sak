#pragma once

#include <cstdio>

#define sak_log_error(__l, ...) fprintf(stderr, "*** " __l "\n", ##__VA_ARGS__)
#define sak_log(__l, ...)       fprintf(stdout, "" __l "\n", ##__VA_ARGS__)

