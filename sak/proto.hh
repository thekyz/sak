#pragma once

#include <google/protobuf/message.h>

#include <pthread.h>

#include "mutex.hh"

#include <string>
#include <cstdlib>
#include <list>
#include <vector>
#include <map>

namespace sak { namespace proto {

/**
 * @file proto.hh
 * @brief protobuf/nanomsg communication wrapper.
 *
 * This interface provides you with tools to help unify and (hopefully) simplify
 * the use of protobuf and nanomsg for communication between applications.
 *
 * For simplicity we will refer to applications "creating" the channel as servers
 * while the applications using the channel will be refered as clients.
 *
 * overview
 * --------
 *
 *        +------+                                      +------+
 *        |client|                  +                   |server|
 *        +------+                  |                   +------+
 *                                  |
 *            send_request()        |          on_request()
 *           +--------------+------------------------------>
 *            on_sent()     |       |                       +
 *           <--------------+       |                       |
 *                                  |                       |
 *                                  |                       |
 *            on_response()         |       send_response() v
 *           <------------------------------+--------------+
 *                                  |       |     on_sent()
 *                                  |       +-------------->
 *                                  |
 *            on_request()          |         send_oneway()
 *           <------------------------------+--------------+
 *                                  |       |     on_sent()
 *                                  |       +-------------->
 *                                  |
 *            send_oneway()         |          on_request()
 *           +--------------+------------------------------>
 *            on_sent()     |       |
 *           <--------------+       |
 *                                  |
 *                                  +
 *
 * client
 * ------
 *
 * The client application must do 2 things to use sak::proto:
 * - have an instance of sak::proto::client<> specialized for the type of messages
 *      sent by the server.
 * - implement a listener interface for the type of messages sent by the server
 *      by inheriting from the sak::proto::listener<> class.
 *
 * For an example as how to implement the client class, look at the blueprint_client
 * class implementation in the blueprint/client application.
 *
 * server
 * ------
 *
 * The server application must do 2 things to use sak::proto:
 * - have an instance of sak::proto::server<> specialized for the type of messages
 *      sent by the client
 * - implement a listener interface for the type of messages sent by the client
 *      by inheriting from the sak::proto::listener<> class.
 *
 * For an example as how to implement the server class, look at the blueprint_server
 * class implementation in the blueprint/server application.
 *
 * error handling
 * --------------
 *
 * send_request() / send_oneway() / send_response()
 *      On success, those methods will return a yevent handle that should be used to
 *      set a specific application timeout based on what is expected to happen.
 *      Success here means that either the message was written to the socket, or was queued
 *      to be written later on.
 *      The timeout applies differently depending on the method:
 *          - send_request() : the event will run its course from the moment the timeout
 *              is set (event->set_timeout() is called) by the application to the moment
 *              a response is received from the receiver
 *          - send_oneway() / send_response() : then event will run its course from the
 *              moment the timeout is set (event->set_timeout() is called) by the
 *              application to the moment the message is successfully written to the
 *              socket.
 *      On error, those methods return NULL. This can happen because:
 *          - the connection is not open
 *          - the given protobuf message could not be serialized
 *          - memory could not be allocated for internal sak::proto context
 *          - any error returned by yipc_send_* (see nanomsg documentation for details)
 *          - sepcific to send_response() : called twice with the same yipc_message_handle_t
 *
 * on_sent()
 *      This is called following a successful send_* method (returning a non NULL event
 *      handler).
 *      on_sent provides you with a yevent_reason code informing you of what happened:
 *          - YEVENT_OCCURED : the message was actually written to the socket
 *          - YEVENT_FAILURE : the message could not be written to the socket
 *          - YEVENT_TIMEOUT : timeout occured (see send_* timeout explanation above)
 *          - YEVENT_CANCEL : application canceled the event (not triggered by yapi)
 *
 * on_response()
 *      This is called following a successful send_request() method call.
 *      on_response provides you with a yevent_reason code informing you of what happened:
 *          - YEVENT_OCCURED : a response to the original request was received and
 *              successfully unpacked
 *          - YEVENT_FAILURE : a response to the original request was received but
 *              sak::proto could not unpack it
 *          - YEVENT_TIMEOUT : timeout occured (see send_* timeout explanation above)
 *          - YEVENT_CANCEL : application canceled the event (not triggered by yapi)
 *
 * on_request()
 *      This is called when a message is received from a peer with a valid unpacked protobuf
 *      protobuf message.
 */

class connection;

/**
 * sak::proto::nt_listener interface declaration.
 *
 * Internal class used as non-typed abstraction by sak::proto::connection.
 * User should implement this interface through sak::proto::listener only.
 */
class nt_listener
{
    friend class connection;

    protected:
        /**
         * Abstraction interface for typed listeners.
         */
        virtual void on_request() = 0;
        virtual google::protobuf::Message *msg() = 0;

    public:
        virtual ~nt_listener() {}

        /**
         * Called upon peer connection.
         */
        virtual void on_connect() { }

        /**
         * Called upon peer disconnection.
         */
        virtual void on_disconnect() { }

        /**
         * Called after a message was sent or if it failed to be sent.
         *
         * @param ctx The context that was passed to the proto::peer->send_*() function.
         */
        virtual void on_sent(void *ctx) { }
};

/**
 * sak::proto::listener interface declaration.
 *
 * This is the interface to implement to receive events from a sak::proto::server or a sak::proto::client.
 * This is typed to the protobuf message you want to send over those channels.
 */
template <class T> class listener : public nt_listener
{
    friend class connection;

    private:
        T m_msg;

    protected:
        virtual void on_request() {
            on_request(m_msg);
        }

        virtual google::protobuf::Message *msg() { return &m_msg; }

    public:
        virtual ~listener() {}

        /**
         * Callback called by sak::proto::connection when a message is received.
         *
         * @param msg The msg itself (only valid for the duration of that call).
         */
        virtual void on_request(const T &msg) = 0;
};

/**
 * sak::proto::proxy class declaration.
 *
 * Handles injection and debug for sak::proto.
 * Should not be used by applications.
 */
class proxy
{
    public:
        static proxy *instance() { return &s_instance; }

        void register_connection(const google::protobuf::Message *msg, connection *conn);
        void unregister_connection(connection *conn);

        std::vector<std::string> detailed_connections();
        nt_listener *get_listener(const std::string &msg_type);

    private:
        static pthread_mutex_t s_lock;
        static proxy s_instance;

        proxy() {}
        ~proxy() {}

        std::map<connection *, std::string> m_connections;
};

/**
 * sak::proto::connection class declaration.
 *
 * Handles the transport part (yipc in this implementation) as well as the packing/unpacking of protobuf messages.
 * Should not be used directly by applications (see sak::proto::client and sak::proto::server for usable implementations).
 */
class connection
{
    friend class peer;
    friend class proxy;

    private:
        /**
         * Connection states (internal use only).
         */
        enum state {
            UNKNOWN = 0,
            UNINITIALIZED,
            OPENED
        };

        /**
         * Open the current connection.
         * This will abort the current process if it cannot succeed.
         */
        void safe_open();

        /**
         * Close the current connection.
         */
        void safe_close();

    protected:
        /**
         * Create an instance of the connection class.
         *
         * @param connection_type NN_SURVEYOR or NN_RESPONDENT
         * @param url Transport protocol and address to use for that connection.
         * @param listener A listener to send received messages to.
         */
        connection(int connection_type, std::string url, nt_listener *listener);

        int m_connection_type;
        std::string m_url;
        nt_listener *m_listener;
        int m_socket;
        int m_endpoint_id;
        state m_state;
        pthread_mutex_t m_listener_lock;

    public:
        /**
         * Destroy an instance of the connection class.
         */
        virtual ~connection();

        /**
         * Is this connection connected to another endpoint?
         * @returns true if yes, false if no.
         */
        virtual bool is_connected() = 0;

        /**
         * Send a message on the bus.
         *
         * @param endpoint The peer that sent the request in the first place.
         * @param msg The message to send.
         * @param yipc_msg_handle The response handle (given by on_request).
         * @param user_ctx Context that will be passed to the on_sent callback.
         * @return The number of bytes in the message on success, -1 on error (and errno is set).
         */
        int send(const google::protobuf::Message *msg, void *user_ctx);
};

/**
 * sak::proto::nt_server class declaration.
 *
 * Private non-typed interface used as server abstraction by the connection class.
 */
class nt_server : public connection
{
    public:
        virtual bool is_connected();

    protected:
        nt_server(std::string url, nt_listener *l)
            : connection(NN_SURVEYOR, url, l) { }
};

/**
 * sak::proto::nt_client class declaration.
 *
 * Private non-typed interface used as client abstraction by the connection class.
 */
class nt_client : public connection
{
    public:
        virtual bool is_connected();

    protected:
        nt_client(std::string url, nt_listener *l)
            : connection(NN_RESPONDENT, url, l)  { }
};

/**
 * sak::proto::server class declaration.
 *
 * This class is used to open a connection that a sak::proto::client can connect to by using the same type of protobuf messages.
 * @see nanomsg survey
 */
template <class T> class server : public nt_server
{
    public:
        /**
         * Create an instance of the server class.
         *
         * @param url Transport protocol and address to use for that connection.
         * @param l The listener to register with that server.
         */
        server(std::string url, listener<T> *l)
            : nt_server(url, l) { }
};

/**
 * sak::proto::client class declaration.
 *
 * This class is used to connect to a sak::proto::server dealing the same type of protobuf messages.
 * @see nanomsg survey
 */
template <class T> class client : public nt_client
{
    public:
        /**
         * Create an instance of the client class.
         *
         * @param url Transport protocol and address to use for that connection.
         * @param l The listener to register with that client.
         */
        client(std::string url, listener<T> *l)
            : nt_client(url, l) { }
};

} }

