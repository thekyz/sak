#ifndef __YPROTO_DBG_H__
#define __YPROTO_DBG_H__

  #ifdef __cplusplus
    extern "C" {
  #endif

/**
 * Initialize the yproto dbg module
 * \returns Always 0.
 */
int yproto_dbg_init(void);

  #ifdef __cplusplus
    }
  #endif

  #ifdef __cplusplus
namespace yproto {
    class injector : public peer {
        public:
            injector(nt_listener *l);
            virtual ~injector();

            static void on_event(yevent_reason_t reason, void *ctx);

            yevent_t *send_response(const google::protobuf::Message *msg, yipc_message_handle_t yipc_msg_handle, void *user_ctx);
            yevent_t *send_request(const google::protobuf::Message *msg, void *user_ctx);
            yevent_t *send_oneway(const google::protobuf::Message *msg, void *user_ctx);

            const bool has_response() const { return m_has_response; }
            const std::string &response_json() const { return m_response_json; }

        private:
            bool m_has_response;
            std::string m_response_json;
            yevent_t *m_response_event;
            nt_listener *m_listener;
            void *m_user_ctx;
    };
};
  #endif


#endif // __YPROTO_DBG_H__

