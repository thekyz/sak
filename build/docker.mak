ifneq (1,$(IN_DOCKER))

.PHONY: all
all: docker-prepare
	$(ECHO)docker exec -ti -u $(USER_ID):$(USER_GROUP) $(BUILDER_CONTAINER) make --no-print-directory -C /w/$(CURDIR_REL) $(MAKECMDGOALS) IN_DOCKER=1 VERBOSE=$(VERBOSE)

docker-prepare:
	$(ECHO)docker inspect -f {{.State.Running}} $(BUILDER_CONTAINER) >/dev/null || \
		docker run -t -d --rm --security-opt seccomp=unconfined --cap-add=SYS_PTRACE --name $(BUILDER_CONTAINER) -u $(USER_ID):$(USER_GROUP) -v $(ROOT):/w -w /w $(BUILDER_CONTAINER):$(VERSION)

docker-build: docker-kill
	$(ECHO)docker build -t $(BUILDER_CONTAINER):$(VERSION) $(ROOT)/build

docker-kill:
	$(ECHO)docker kill $(BUILDER_CONTAINER) || true

docker-pull:
	$(ECHO)echo "TODO ..."

endif
