include ../../build/common.mak

ifeq (1,$(IN_DOCKER))

.PHONY: app
all: .track_dependencies
	@echo ">>> Building $(CURDIR_REL) ..."
	$(ECHO)$(CMD_BUILD) $(CMD_BUILD_TARGETS)

endif
