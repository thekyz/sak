ifeq (1,$(VERBOSE))
CMD_TEST_VERBOSITY=--test_output=all
else
CMD_TEST_VERBOSITY=--test_output=errors
endif

CMD_TEST=bazel test
CMD_TEST_TARGETS=//...

CMD_BUILD=bazel build
CMD_BUILD_TARGETS=.
