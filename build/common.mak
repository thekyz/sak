ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
ROOT=$(shell git rev-parse --show-toplevel)
VERSION=$(shell cat $(ROOT)/VERSION)
CURDIR_REL=$(shell realpath --relative-to $(ROOT) $(CURDIR)) 
USER_ID=$(shell id -u)
USER_GROUP=$(shell id -g)

ifeq (1,$(VERBOSE))
ECHO=

$(info ROOT_DIR=$(ROOT_DIR))
$(info ROOT=$(ROOT))
$(info CURDIR=$(CURDIR))
$(info VERSION=$(VERSION))
$(info USER_ID=$(USER_ID))
$(info USER_GROUP=$(USER_GROUP))
else
ECHO=@
endif

BUILDER_CONTAINER=sak-builder

ifneq (1,$(IN_DOCKER))

include $(ROOT)/build/docker.mak

else # INDOCKER=1

ifeq (1,$(CMAKE))
BUILD_SYSTEM=cmake
else
BUILD_SYSTEM=bazel
endif

include $(ROOT)/build/$(BUILD_SYSTEM)/build.mak

.track_dependencies:
	@echo ">>> Tracking dependencies from $(CURDIR_REL) ..."
	$(ECHO)$(ROOT)/build/gen_build_files.py

endif # INDOCKER
