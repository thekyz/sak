#pragma once

#include <stdlib.h>

#define __UID2(__a, __b)    __a ## __b
#define __UID1(__a, __b)    __UID2(__a, __b)
#define __UID               __UID1(__it, __LINE__)

struct ll {
    void *data;
    struct ll *next;
};

#define ll_foreach(__ll, __it)      struct ll *__UID = (__ll); for (__it = *(__typeof__(__it) *)(__UID)->data; (__UID)->next; (__UID) = (__UID)->next, __it = *(__typeof__(__it) *)(__UID)->data)

/** Generic callback type. **/
typedef void (*ll_cb)(void *data);

/**
 * Allocates a node.
 * @return the newly allocated node on success, NULL otherwise
 */
static inline struct ll *ll_create(void *data)
{
    struct ll *n = (struct ll *)malloc(sizeof(struct ll));
    if (n != NULL) {
        n->data = data;
        n->next = NULL;
    }

    return n;
}

/**
 * Deallocates a node (also deallocates nodes linked to it) and calls a callback for each node.
 */
static inline void ll_free(struct ll *l, ll_cb on_free)
{
    if (!l) {
        return;
    }

    if (on_free) {
        on_free(l->data);
    }

    if (l->next) {
        ll_free(l->next, on_free);
    }

    free(l);
}

/**
 * Calls a callback for each element in the list.
 */
static inline void ll_traverse(struct ll *l, ll_cb on_data)
{
    if (!l || !on_data || !l->data) {
        return;
    }

    struct ll *n = l;
    while (n) {
        on_data(n->data);
        n = n->next;
    }
}

/**
 * Add a new node at the end of the list.
 */
static inline struct ll *ll_push_tail(struct ll *l, void *data)
{
    struct ll *n = l;
    if (n->data == NULL) {
        n->data = data;
        return n;
    }

    while (n->next) {
       n = n->next;
    }

    n->next = ll_create(data);
    if (n->next == NULL) {
        return NULL;
    }

    return n->next;
}

/**
 * Remove the node from the start of the list.
 * @return the data from the node; NULL on error.
 */
static inline void *ll_pop_head(struct ll *l)
{
    if (!l) {
        return NULL;
    }

    void *data = l->data;
    if (l->next == NULL) {
        l->data = NULL;
    } else {
        struct ll *next = l->next;
        l->data = next->data;
        l->next = next->next;
        free(next);
    }

    return data;
}

/**
 * Remove the specied node from the list
 * @param l the list
 * @param n the node to remove
 * @return the data from the removed node
 */
static void *ll_pop_node(struct ll *l, struct ll *n)
{
    if (!l || !n) {
        return NULL;
    }

    if (l == n) {
        return ll_pop_head(l);
    }

    struct ll *c = l;

    while (c) {
        if (c->next == n) {
            c->next = c->next->next;

            void *data = n->data;
            free(n);

            return data;
        }

        c = c->next;
    }

    return NULL;
}

/**
 * Get current size.
 * @return 0 if list with 1 node and no data; -1 on error
 */
static inline int ll_size(struct ll *l)
{
    if (!l) {
        return -1;
    }

    struct ll *n = l;
    if (n->data == NULL) {
        return 0;
    }

    size_t count = 1;
    while (n->next) {
       n = n->next;
       count++;
    }

    return count;
}

