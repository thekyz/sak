#include <unity.h>

#include "utils/ll.h"

int _dummy[] = { 0x1, 0x2, 0x3, 0x4, 0x5, 0x6 };

int _ll_cb_call_count = 0;
static void _test_ll_cb(void *data)
{
    TEST_ASSERT_EQUAL(++_ll_cb_call_count, *(int *)data);
}

static void test_ll_traverse()
{
    struct ll *node = ll_create(NULL);
    for (int it = 0; it < (int)(sizeof(_dummy)/sizeof(*_dummy)); it++) {
        TEST_ASSERT_NOT_NULL(ll_push_tail(node, &_dummy[it]));
    }

    ll_traverse(node, _test_ll_cb);
    TEST_ASSERT_EQUAL(_ll_cb_call_count, sizeof(_dummy)/sizeof(*_dummy));
}

static void test_ll_foreach()
{
    struct ll *node = ll_create(NULL);
    for (int it = 0; it < (int)(sizeof(_dummy)/sizeof(*_dummy)); it++) {
        TEST_ASSERT_NOT_NULL(ll_push_tail(node, &_dummy[it]));
    }

    int index = 0;

    int node_data;
    ll_foreach(node, node_data) {
        TEST_ASSERT_EQUAL(node_data, _dummy[index++]);
    }

    TEST_ASSERT_EQUAL(index + 1, sizeof(_dummy)/sizeof(*_dummy));
}

static void test_ll_pop_node()
{
    struct ll *list = ll_create(NULL);
    struct ll *nodes[] = { NULL, NULL, NULL, NULL, NULL, NULL };
    for (int it = 0; it < (int)(sizeof(_dummy)/sizeof(*_dummy)); it++) {
        nodes[it] = ll_push_tail(list, &_dummy[it]);
    }

    TEST_ASSERT_EQUAL(&_dummy[4], ll_pop_node(list, nodes[4]));
    TEST_ASSERT_EQUAL(5, ll_size(list));
    TEST_ASSERT_EQUAL(&_dummy[3], ll_pop_node(list, nodes[3]));
    TEST_ASSERT_EQUAL(4, ll_size(list));
    TEST_ASSERT_EQUAL(&_dummy[2], ll_pop_node(list, nodes[2]));
    TEST_ASSERT_EQUAL(3, ll_size(list));
    TEST_ASSERT_EQUAL(&_dummy[5], ll_pop_node(list, nodes[5]));
    TEST_ASSERT_EQUAL(2, ll_size(list));
    TEST_ASSERT_EQUAL(&_dummy[1], ll_pop_node(list, nodes[1]));
    TEST_ASSERT_EQUAL(1, ll_size(list));
    TEST_ASSERT_EQUAL(&_dummy[0], ll_pop_node(list, nodes[0]));
    TEST_ASSERT_EQUAL(0, ll_size(list));
}

static void test_ll_create_with_data()
{
    int dummy = 0x1;

    struct ll *node = ll_create(&dummy);
    TEST_ASSERT_NOT_NULL(node);
    TEST_ASSERT_EQUAL(*(int *)node->data, dummy);
}

static void test_ll_create_no_data()
{
    struct ll *node = ll_create(NULL);
    TEST_ASSERT_NOT_NULL(node);
    TEST_ASSERT_NULL(node->data);
}

static void test_ll_push_tail_after_create_with_data()
{
    int dummy1 = 0x1;
    struct ll *node = ll_create(&dummy1);
    TEST_ASSERT_NOT_NULL(node);

    int dummy2 = 0x2;
    struct ll *pushed_node = ll_push_tail(node, &dummy2);
    TEST_ASSERT_NOT_NULL(pushed_node);
    TEST_ASSERT_NOT_EQUAL(pushed_node, node);
    TEST_ASSERT_EQUAL(*(int *)pushed_node->data, dummy2);

    TEST_ASSERT_EQUAL(ll_size(node), 2);
}

static void test_ll_push_tail_after_create_no_data()
{
    struct ll *node = ll_create(NULL);
    TEST_ASSERT_NOT_NULL(node);

    int dummy = 0x2;
    struct ll *pushed_node = ll_push_tail(node, &dummy);
    TEST_ASSERT_NOT_NULL(pushed_node);
    TEST_ASSERT_EQUAL(pushed_node, node);
    TEST_ASSERT_EQUAL(*(int *)pushed_node->data, dummy);

    TEST_ASSERT_EQUAL(ll_size(node), 1);
}

static void test_ll_pop_head_empty_list()
{
    struct ll *node = ll_create(NULL);
    TEST_ASSERT_NOT_NULL(node);

    TEST_ASSERT_NULL(ll_pop_head(node));
}

static void test_ll_pop_head_big_list()
{
    int dummy[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 ,10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

    struct ll *node = ll_create(NULL);
    TEST_ASSERT_NOT_NULL(node);

    for (size_t i = 0; i < sizeof(dummy) / sizeof(*dummy); i++) {
        struct ll *pushed_node = ll_push_tail(node, &dummy[i]);
        TEST_ASSERT_EQUAL(*(int *)pushed_node->data, dummy[i]);
    }

    int *popped_data = NULL;
    size_t pop_count = 0;
    while ((popped_data = ll_pop_head(node))) {
        TEST_ASSERT_EQUAL(*popped_data, dummy[pop_count]);
        pop_count++;
    }

    TEST_ASSERT_EQUAL(pop_count, 20);
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_ll_traverse);
    RUN_TEST(test_ll_foreach);
    RUN_TEST(test_ll_pop_node);
    RUN_TEST(test_ll_create_with_data);
    RUN_TEST(test_ll_create_no_data);
    RUN_TEST(test_ll_push_tail_after_create_with_data);
    RUN_TEST(test_ll_push_tail_after_create_no_data);
    RUN_TEST(test_ll_pop_head_empty_list);
    RUN_TEST(test_ll_pop_head_big_list);
    return UNITY_END();
}

