#include <unity.h>

#include "utils/list_l.h"

struct data {
    list node;
    int data;
};

struct data _dummy[] = {
    {.data=0x1}, {.data=0x2}, {.data=0x3}, {.data=0x4}, {.data=0x5}, {.data=0x6}
};

static void test_list_foreach()
{
    list l;
    list_init(&l);

    for (int it = 0; it < (int)(sizeof(_dummy)/sizeof(*_dummy)); it++) {
        list_add_tail(&l, &_dummy[it].node);
    }

    int index = 0;

    struct data *data_it;
    list_foreach(&l, data_it) {
        TEST_ASSERT_EQUAL(data_it->data, _dummy[index++].data);
    }

    TEST_ASSERT_EQUAL(index, sizeof(_dummy)/sizeof(*_dummy));
}

static void test_list_delete()
{
    list l;
    list_init(&l);

    for (int it = 0; it < (int)(sizeof(_dummy)/sizeof(*_dummy)); it++) {
        list_add_tail(&l, &_dummy[it].node);
    }

    list_delete(&_dummy[4].node);
    TEST_ASSERT_EQUAL(5, list_count(&l));
    list_delete(&_dummy[3].node);
    TEST_ASSERT_EQUAL(4, list_count(&l));
    list_delete(&_dummy[2].node);
    TEST_ASSERT_EQUAL(3, list_count(&l));
    list_delete(&_dummy[5].node);
    TEST_ASSERT_EQUAL(2, list_count(&l));
    list_delete(&_dummy[1].node);
    TEST_ASSERT_EQUAL(1, list_count(&l));
    list_delete(&_dummy[0].node);
    TEST_ASSERT_EQUAL(0, list_count(&l));
}

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_list_foreach);
    RUN_TEST(test_list_delete);
    return UNITY_END();
}
