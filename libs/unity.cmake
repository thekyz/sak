
set(UNITY_PREFIX unity)
set(UNITY_URL https://github.com/ThrowTheSwitch/Unity/archive/v2.4.3.tar.gz)
set(UNITY_VERSION v2.4.3)
set(UNITY_SHA256 a8c5e384f511a03c603bbecc9edc24d2cb4a916998d51a29cf2e3a2896920d03)

ExternalProject_Add(${UNITY_PREFIX}
    URL ${UNITY_URL}
    URL_HASH SHA256=${UNITY_SHA256}
    UPDATE_COMMAND ""
    PATCH_COMMAND ""
    INSTALL_COMMAND ""
    CONFIGURE_COMMAND ""
    TEST_COMMAND ""
    BUILD_COMMAND ""
)

ExternalProject_Get_property(unity SOURCE_DIR)

set(unity_INCLUDE_DIRS "${SOURCE_DIR}/src/")
set(unity_LIBRARIES "libunity")
include_directories(${unity_INCLUDE_DIRS})

set(unity_SOURCE_FILES ${SOURCE_DIR}/src/unity.c)
set_source_files_properties(${unity_SOURCE_FILES} PROPERTIES GENERATED TRUE)
add_library(libunity ${unity_SOURCE_FILES})
